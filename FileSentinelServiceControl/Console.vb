﻿Imports System.ServiceProcess
Imports System.Diagnostics
Public Class Console
    Private Const Service_name As String = "FileSentinelService"
    Private sc As ServiceController
    Private ReadOnly Property ServiceController As ServiceController
        Get
            If sc Is Nothing Then
                sc = New ServiceController(Service_name)
                Try
                    If sc.CanStop Then
                    End If
                Catch
                    Try
                        If tmrStatus IsNot Nothing Then
                            tmrStatus.Stop()
                            tmrStatus.Dispose()
                            tmrStatus = Nothing
                        End If
                    Catch
                    End Try
                    SetStatus("Not Installed/Broken")
                End Try
            End If
            Return sc
        End Get
    End Property

    Private tmrStatus As Timers.Timer
    Private ReadOnly Property StatusTimer As Timers.Timer
        Get
            If tmrStatus Is Nothing Then
                tmrStatus = New Timers.Timer(1000)
                AddHandler tmrStatus.Elapsed, Sub()
                                                  StatusTimer_Elapsed()
                                              End Sub
            End If
            Return tmrStatus
        End Get
    End Property

    Private Delegate Sub EmptyDelegate()
    Private Sub StatusTimer_Elapsed()
        ServiceController.Refresh()

        Try
            SetStatus(ServiceController.Status.ToString)
        Catch ex As Exception
            Try
                If tmrStatus IsNot Nothing Then
                    tmrStatus.Stop()
                    tmrStatus.Dispose()
                    tmrStatus = Nothing
                End If
            Catch
            End Try
            SetStatus("Can't Find Service")
        End Try
    End Sub
    Private Sub SetStatus(status As String)
        If Me.InvokeRequired Then
            Dim del As New EmptyDelegate(Sub()
                                             lblStatus.Text = status
                                         End Sub)
            Me.Invoke(del)
        Else
            lblStatus.Text = status
        End If
    End Sub

    Private Sub console_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If Not Me.DesignMode Then
            StatusTimer.Start()
        End If
    End Sub
    Private Delegate Sub StringDelegate(val As String)

    Private Sub BtnStop_Click(sender As Object, e As EventArgs) Handles BtnStop.Click
        Try
            Shell("SC Stop FileSentinelService")
            MessageBox.Show("Service Stopping!")
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub BtnStart_Click(sender As Object, e As EventArgs) Handles BtnStart.Click
        Try
            Shell("SC Start FileSentinelService")
            MessageBox.Show("Service Starting!")
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Process.GetCurrentProcess().Kill()

    End Sub
End Class